var hardhat = {
	/* do something */
	showDate: function() {
		var d = new Date();

		var dayofweeks = new Array();
		dayofweeks[0] = "Sun";
		dayofweeks[1] = "Mon";
		dayofweeks[2] = "Tue";
		dayofweeks[3] = "Wed";
		dayofweeks[4] = "Thu";
		dayofweeks[5] = "Fri";
		dayofweeks[6] = "Sat";

		var months = new Array();
		months[0] = "Jan";
		months[1] = "Feb";
		months[2] = "Mar";
		months[3] = "Apr";
		months[4] = "May";
		months[5] = "Jun";
		months[6] = "Jul";
		months[7] = "Aug";
		months[8] = "Sep";
		months[9] = "Oct";
		months[10] = "Nov";
		months[11] = "Dec";

		var dayofweek = dayofweeks[d.getDay()];
		var dayOfMonth = d.getDate();
		var month = months[d.getMonth()];
	    $("#dayofweek").html(dayofweek);
	    $("#dayofmonth").html(dayOfMonth);
	    $("#month").html(month);
	},

	/* initialise functions */
	init: function() {
		this.showDate();
	}
}

$(document).ready(function() {
	hardhat.init();
});